﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using DB.Enties;

namespace DB.EntiesMap
{
    class LoansGrantedMap:ClassMap<LoansGranted>
    {
        public LoansGrantedMap()
        {
            Id(e => e.loanNumber).Length(26);
            Map(e => e.loanData);
            Map(e => e.loanAmount);
            Map(e => e.partnerUnit);
            Map(e => e.Pesel);
            Map(e => e.commission);
        }
    }
}
