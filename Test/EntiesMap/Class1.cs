﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Context;
using NHibernate.Event;
using Configuration = NHibernate.Cfg.Configuration;

namespace DB
{
    public static class NHibernateHelper
    {
        private static readonly string SerializedConfig = string.Format("NHConfiguration.{0}.{1}.serialized", Assembly.GetExecutingAssembly().GetName().Version, ConnectionStringSettings.Name);
        private static readonly string ConfigPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SerializedConfig);

        public static ConnectionStringSettings ConnectionStringSettings
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[Environment.MachineName] ??
                       ConfigurationManager.ConnectionStrings["default"];
            }
        }

        public static ISessionFactory CreateSessionFactory()
        {
            return CreateSessionFactory(null);
        }

        public static ISessionFactory CreateSessionFactory(ConnectionStringSettings connectionStringSettings)
        {
            var con = connectionStringSettings == null ? ConnectionStringSettings.ConnectionString : connectionStringSettings.ConnectionString;
            var msSqlConfiguration = MsSqlConfiguration.MsSql2008.ConnectionString(con);
#if DEBUG
            msSqlConfiguration.ShowSql().FormatSql();
#endif

            return CreateNHConfig(msSqlConfiguration).BuildSessionFactory();
        }


        public static Configuration CreateNHConfig(MsSqlConfiguration msSqlConfiguration)
        {
            var serializer = new BinaryFormatter();
            Configuration returnConfig;

            if (File.Exists(ConfigPath)/* && IsConfigurationFileValid()*/)
            {
                using (Stream stream = File.OpenRead(ConfigPath))
                {
                    returnConfig = serializer.Deserialize(stream) as Configuration;
                }
            }
            else
            {
                returnConfig = Fluently.Configure()
                    .Database(msSqlConfiguration)
                    .Mappings(m => m.FluentMappings
                                       .AddFromAssembly(Assembly.Load("MyProject.Data"))
                                       .Conventions.Add(
                                           Table.Is(x => "tbl" + x.EntityType.Name),
                                           PrimaryKey.Name.Is(x => x.EntityType.Name + "Id"),
                                           ForeignKey.EndsWith("Id")
                                       )
                    )
                    .CurrentSessionContext<WebSessionContext>()
                    .Cache(c => c.UseQueryCache().ProviderClass<HashtableCacheProvider>())
#if DEBUG
                    .ExposeConfiguration(cfg => cfg.SetProperty("generate_statistics", "true"))
#endif
                    .BuildConfiguration();

                using (Stream stream = File.OpenWrite(ConfigPath))
                {
                    serializer.Serialize(stream, returnConfig);
                }
            }
            return returnConfig;
        }


        private static bool IsConfigurationFileValid()
        {
            var assInfo = new FileInfo(ConfigPath);
            return assInfo.LastWriteTime >= File.GetCreationTime(Assembly.GetExecutingAssembly().Location);
        }
    }
}
