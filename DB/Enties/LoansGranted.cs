﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB.Enties
{
    public class LoansGranted
    {
        [System.ComponentModel.DisplayName("Numer Kredytu")]
        public virtual string loanNumber { get; set; }

        [System.ComponentModel.DisplayName("Kwota Kredytu")]
        public virtual double loanAmount { get; set; }

        [System.ComponentModel.DisplayName("Data Kredytu")]
        public virtual string loanData { get; set; }

        [System.ComponentModel.DisplayName("Pesel Kredytobiorcy")]
        public virtual long Pesel { get; set; }

        [System.ComponentModel.DisplayName("Jednostka Partnerska")]
        public virtual int partnerUnit { get; set; }
        [System.ComponentModel.DisplayName("Naliczona Prowizja")]
        [System.ComponentModel.TypeConverter(typeof(Forms.DataGridViewCellStyleConverter))]

        public virtual decimal commission { get; set; } = 0;


        public LoansGranted()
        {

        }
        public LoansGranted(string fromString)
        {
            var s = fromString.Split(';');
            this.loanNumber = s[0];
            this.loanAmount = double.Parse(s[1]);
            this.loanData = s[2];
            Pesel = long.Parse(s[3]);
            this.partnerUnit = int.Parse(s[4]);
        }
        public virtual void CalcCommission(double percent)
        {
            this.commission = Math.Round(((decimal)loanAmount / (decimal)100) * (decimal)percent, 2, MidpointRounding.AwayFromZero);
        }


    }
}
