﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;




namespace DB
{
   
    public static class DB 
    {

        public static ISessionFactory sessionFactory = CreateSessionFactory();
        #region creat session factory region

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                   .Database(CreateDbConfig())
                   .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                   //.ExposeConfiguration(DropCreateSchema)
                   .BuildSessionFactory();
        }

        private static MySQLConfiguration CreateDbConfig()
        {//TODO: resolve missing system.configuration
            return MySQLConfiguration.Standard.ConnectionString(c => c.Database("loanlist").Server("localhost").Username("root").Password(""));

        }

        private static void DropCreateSchema(NHibernate.Cfg.Configuration cfg)
        {
            new SchemaExport(cfg)
            .Create(false, true);

        }
        #endregion
    }
}
