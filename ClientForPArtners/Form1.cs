﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB.Enties;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ClientForPArtners
{
    using static LoanController;
    public partial class Form1 : Form
    {
        private List<LoansGranted> filtertloans = loans;
        public Form1()
        {
            InitializeComponent();
            InitBindings();
            InitComboxValue();
            InitChart();
        }

        private void InitComboxValue()
        {
            periotBox.Items.Add("");

            periotBox.Items.AddRange(filtertloans.Select(z => z.loanData.Remove(7)).Distinct().ToArray());

            parnerNumberBox.Items.Add("");
            parnerNumberBox.Items.AddRange(filtertloans.Select(z => z.partnerUnit.ToString()).Distinct().ToArray());
            //MessageBox.Show("asd");
           // parnerNumberBox.SelectedIndex = 5;
        }

        private void RefreshData()
        {
            if (parnerNumberBox.Text != "" && periotBox.Text != "")
            {
                filtertloans = loans.Where(c => c.partnerUnit == int.Parse(parnerNumberBox.Text)
                                               && c.loanData.Remove(7) == periotBox.Text)
                                   .ToList();
            }
            else
            {
                if (parnerNumberBox.Text != "" || periotBox.Text != "")
                {
                    filtertloans = loans.Where(c => c.partnerUnit == int.Parse(parnerNumberBox.Text == ""? "0": parnerNumberBox.Text)
                                              || c.loanData.Remove(7) == periotBox.Text)
                                  .ToList();
                }
                else
                {
                    filtertloans = loans;
                }
            }
            label4.Text = string.Format("{0:c}", filtertloans.Sum(e => e.commission));
            InitBindings();
            InitChart();
        }

        private void InitChart()
        {
            var chartHandler = (Chart)chartPage.Controls[0];
            var creditAmmountInMounth = filtertloans.GroupBy(e => e.loanData.Remove(7))
                                                    .Distinct()
                                                    .Select(e => new { e.Key, comm = e.Sum(z => z.commission) })
                                                    .ToList();

            chartHandler.Series[0].Points.DataBindXY(creditAmmountInMounth.Select(e=>e.Key).ToArray(),creditAmmountInMounth.Select(e=>e.comm).ToArray());

        }

        private void RefreshComboBox(object sender, EventArgs e)
        {
            RefreshData();
            //var _sender = ((ComboBox)sender);
            //if (_sender.Name == parnerNumberBox.Name)
            //{
            //    if (parnerNumberBox.Text != "")
            //    {//reset default values
            //        periotBox.Items.Clear();
            //        periotBox.Items.Add("");
            //        periotBox.Items.AddRange(filtertloans.Where(a => a.partnerUnit.ToString() == parnerNumberBox.Text)
            //                                             .Select(z => z.loanData.Remove(7))
            //                                             .Distinct()
            //                                             .ToArray());
            //    }
            //    else
            //    {
            //        periotBox.Items.Clear();
            //        periotBox.Items.Add("");
            //        periotBox.Items.AddRange(loans.Select(z => z.loanData.Remove(7)).Distinct().ToArray());
            //    }
            //}
            //else
            //{
            //    if (parnerNumberBox.Text == "")
            //    {

            //        periotBox.clear
            //        periotBox.Items.Add("");

            //        periotBox.Items.AddRange(loans.Select(z => z.loanData.Remove(7)).Distinct().ToArray());
            //    }
            // }

            //
            //void catchDifrences(object s,string[] text)
            // {
            //     var c = ((ComboBox)s).Items;
            //     try
            //     {
            //         ((ComboBox)s).SelectedIndex=((ComboBox)s).Items.IndexOf(text);
            //     }
            //     catch (Exception)
            //     {

            //         ((ComboBox)s).SelectedIndex = 0;
            //     }
            // }

        }
        private void InitBindings()
        {
            var datagridHandler = (DataGridView)dataGridPage.Controls[0];
            BindingList<LoansGranted> bindingList = new BindingList<LoansGranted>(filtertloans);
            var source = new BindingSource(bindingList, null);
            datagridHandler.DataSource = source;

        }

        private void Chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
