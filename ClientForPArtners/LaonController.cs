﻿using DB.Enties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientForPArtners
{
    using static DB.DB;
    public static class LoanController
    {
        public static List<LoansGranted> loans { get; set; } = Init();
        public static void SaveOrUpdate()
        {

            using (var s = sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    loans.ForEach(e => s.SaveOrUpdate(e));
                    t.Commit();
                }
                s.Close();
            }
        }
        public static void Truncle()
        {
            using (var s = sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    loans.ForEach(e => s.Delete(e));
                    t.Commit();
                }
                s.Close();
            }
        }

        private static List<LoansGranted> Init()
        {
            List<LoansGranted> l;
            using (var s = sessionFactory.OpenSession())
            {
                l = s.Query<LoansGranted>().ToList();
                s.Close();
            }
            return l;
        }
    }
}
