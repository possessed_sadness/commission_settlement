﻿namespace ClientForPArtners
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.parnerNumberBox = new System.Windows.Forms.ComboBox();
            this.periotBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabCase = new System.Windows.Forms.TabControl();
            this.dataGridPage = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.loanNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loanDataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.peselDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnerUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loanAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commissionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loansGrantedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.loansGrantedBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.chartPage = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabCase.SuspendLayout();
            this.dataGridPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loansGrantedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loansGrantedBindingSource1)).BeginInit();
            this.chartPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // parnerNumberBox
            // 
            this.parnerNumberBox.FormattingEnabled = true;
            this.parnerNumberBox.Location = new System.Drawing.Point(29, 66);
            this.parnerNumberBox.Name = "parnerNumberBox";
            this.parnerNumberBox.Size = new System.Drawing.Size(155, 24);
            this.parnerNumberBox.TabIndex = 0;
            this.parnerNumberBox.SelectedIndexChanged += new System.EventHandler(this.RefreshComboBox);
            // 
            // periotBox
            // 
            this.periotBox.FormattingEnabled = true;
            this.periotBox.Location = new System.Drawing.Point(28, 133);
            this.periotBox.Name = "periotBox";
            this.periotBox.Size = new System.Drawing.Size(156, 24);
            this.periotBox.TabIndex = 1;
            this.periotBox.SelectedIndexChanged += new System.EventHandler(this.RefreshComboBox);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Partner numer*:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Za okres*:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Łączna prowizja:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "0.00 pln";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 65);
            this.label5.TabIndex = 6;
            this.label5.Text = "* dla pustej wartości nalicza dla wszystkich możliwych";
            // 
            // tabCase
            // 
            this.tabCase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCase.Controls.Add(this.dataGridPage);
            this.tabCase.Controls.Add(this.chartPage);
            this.tabCase.Location = new System.Drawing.Point(199, 3);
            this.tabCase.Name = "tabCase";
            this.tabCase.SelectedIndex = 0;
            this.tabCase.Size = new System.Drawing.Size(1255, 707);
            this.tabCase.TabIndex = 7;
            // 
            // dataGridPage
            // 
            this.dataGridPage.Controls.Add(this.dataGridView1);
            this.dataGridPage.Location = new System.Drawing.Point(4, 25);
            this.dataGridPage.Name = "dataGridPage";
            this.dataGridPage.Padding = new System.Windows.Forms.Padding(3);
            this.dataGridPage.Size = new System.Drawing.Size(1247, 678);
            this.dataGridPage.TabIndex = 0;
            this.dataGridPage.Text = "tablica danych";
            this.dataGridPage.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.loanNumberDataGridViewTextBoxColumn,
            this.loanDataDataGridViewTextBoxColumn,
            this.peselDataGridViewTextBoxColumn,
            this.partnerUnitDataGridViewTextBoxColumn,
            this.loanAmountDataGridViewTextBoxColumn,
            this.commissionDataGridViewTextBoxColumn});
            this.dataGridView1.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.loansGrantedBindingSource, "commission", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, "0", "C2"));
            this.dataGridView1.DataSource = this.loansGrantedBindingSource1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1241, 672);
            this.dataGridView1.TabIndex = 0;
            // 
            // loanNumberDataGridViewTextBoxColumn
            // 
            this.loanNumberDataGridViewTextBoxColumn.DataPropertyName = "loanNumber";
            this.loanNumberDataGridViewTextBoxColumn.Frozen = true;
            this.loanNumberDataGridViewTextBoxColumn.HeaderText = "Numer Kredytu";
            this.loanNumberDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.loanNumberDataGridViewTextBoxColumn.Name = "loanNumberDataGridViewTextBoxColumn";
            this.loanNumberDataGridViewTextBoxColumn.Width = 170;
            // 
            // loanDataDataGridViewTextBoxColumn
            // 
            this.loanDataDataGridViewTextBoxColumn.DataPropertyName = "loanData";
            this.loanDataDataGridViewTextBoxColumn.Frozen = true;
            this.loanDataDataGridViewTextBoxColumn.HeaderText = "Data Kredytu";
            this.loanDataDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.loanDataDataGridViewTextBoxColumn.Name = "loanDataDataGridViewTextBoxColumn";
            this.loanDataDataGridViewTextBoxColumn.Width = 80;
            // 
            // peselDataGridViewTextBoxColumn
            // 
            this.peselDataGridViewTextBoxColumn.DataPropertyName = "Pesel";
            this.peselDataGridViewTextBoxColumn.Frozen = true;
            this.peselDataGridViewTextBoxColumn.HeaderText = "Pesel Kredytobiorcy";
            this.peselDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.peselDataGridViewTextBoxColumn.Name = "peselDataGridViewTextBoxColumn";
            this.peselDataGridViewTextBoxColumn.Width = 125;
            // 
            // partnerUnitDataGridViewTextBoxColumn
            // 
            this.partnerUnitDataGridViewTextBoxColumn.DataPropertyName = "partnerUnit";
            this.partnerUnitDataGridViewTextBoxColumn.Frozen = true;
            this.partnerUnitDataGridViewTextBoxColumn.HeaderText = "Jednostka Partnerska";
            this.partnerUnitDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.partnerUnitDataGridViewTextBoxColumn.Name = "partnerUnitDataGridViewTextBoxColumn";
            this.partnerUnitDataGridViewTextBoxColumn.Width = 80;
            // 
            // loanAmountDataGridViewTextBoxColumn
            // 
            this.loanAmountDataGridViewTextBoxColumn.DataPropertyName = "loanAmount";
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.loanAmountDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.loanAmountDataGridViewTextBoxColumn.Frozen = true;
            this.loanAmountDataGridViewTextBoxColumn.HeaderText = "Kwota Kredytu";
            this.loanAmountDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.loanAmountDataGridViewTextBoxColumn.Name = "loanAmountDataGridViewTextBoxColumn";
            this.loanAmountDataGridViewTextBoxColumn.Width = 125;
            // 
            // commissionDataGridViewTextBoxColumn
            // 
            this.commissionDataGridViewTextBoxColumn.DataPropertyName = "commission";
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.commissionDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.commissionDataGridViewTextBoxColumn.HeaderText = "Naliczona Prowizja";
            this.commissionDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.commissionDataGridViewTextBoxColumn.Name = "commissionDataGridViewTextBoxColumn";
            this.commissionDataGridViewTextBoxColumn.Width = 125;
            // 
            // loansGrantedBindingSource
            // 
            this.loansGrantedBindingSource.DataSource = typeof(DB.Enties.LoansGranted);
            // 
            // loansGrantedBindingSource1
            // 
            this.loansGrantedBindingSource1.DataSource = typeof(DB.Enties.LoansGranted);
            // 
            // chartPage
            // 
            this.chartPage.Controls.Add(this.chart1);
            this.chartPage.Location = new System.Drawing.Point(4, 25);
            this.chartPage.Name = "chartPage";
            this.chartPage.Padding = new System.Windows.Forms.Padding(3);
            this.chartPage.Size = new System.Drawing.Size(1247, 678);
            this.chartPage.TabIndex = 1;
            this.chartPage.Text = "chart";
            this.chartPage.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.LegendText = "Zysk w czasie";
            series1.Name = "CommInTime";
            series1.YValuesPerPoint = 2;
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(1241, 672);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.Chart1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1466, 707);
            this.Controls.Add(this.tabCase);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.periotBox);
            this.Controls.Add(this.parnerNumberBox);
            this.Name = "Form1";
            this.Text = "commision";
            this.tabCase.ResumeLayout(false);
            this.dataGridPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loansGrantedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loansGrantedBindingSource1)).EndInit();
            this.chartPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox parnerNumberBox;
        private System.Windows.Forms.ComboBox periotBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabCase;
        private System.Windows.Forms.TabPage dataGridPage;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage chartPage;
        private System.Windows.Forms.BindingSource loansGrantedBindingSource;
        private System.Windows.Forms.BindingSource loansGrantedBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn loanNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loanDataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn peselDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnerUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loanAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commissionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

