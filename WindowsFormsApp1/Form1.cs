﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DB.Enties;

namespace WindowsFormsApp1
{
    using static LoanController;

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            if (loans.Count==0)
            {
                InitData();
            }
            BindData();
        }
        private void ValueControll()
        {
            if (periotBox.Text != "" && partnersBox.Text == "")
            {
                partnersBox.Items.Clear();
                partnersBox.Items.Add("");
                partnersBox.Items.AddRange(loans.Where(e=>e.loanData.Remove(7) == periotBox.Text) .Select(e => e.partnerUnit.ToString()).Distinct().ToArray());
            }
            if (periotBox.Text == "" && partnersBox.Text != "")
            {
                periotBox.Items.Clear();
                periotBox.Items.Add("");
                periotBox.Items.AddRange(loans.Where(e=>e.partnerUnit == int.Parse(partnersBox.Text)).Select(e => e.loanData.Remove(7)).Distinct().ToArray());
            }
            if (periotBox.Text == "" && partnersBox.Text == "")
            {
                partnersBox.Items.Clear();
                partnersBox.Items.Add("");
                partnersBox.Items.AddRange(loans.Select(e => e.partnerUnit.ToString()).Distinct().ToArray());
                periotBox.Items.Clear();
                periotBox.Items.Add("");
                periotBox.Items.AddRange(loans.Select(e => e.loanData.Remove(7)).Distinct().ToArray());
            }

        }
        private void RefreshGrid(object sender, EventArgs e)
        {
            if (sender is ComboBox)
            {
                ValueControll();
            }


            var query = loans.Select(c=>c);
            if (partnersBox.Text!="")
            {
              query=  query.Where(el => el.partnerUnit == int.Parse(partnersBox.Text)).ToList();
            }

            if (periotBox.Text !="" )
            {
               query= query.Where(el => el.loanData.Remove(7) == periotBox.Text);
            }
            query = query.OrderByDescending(s => s.loanAmount);
            BindingList<LoansGranted> bindingList = new BindingList<LoansGranted>(query.ToList());


            var source = new BindingSource(bindingList, null);
            dataGridView1.DataSource = source;
        }
        private void BindData()
        {
            RefreshGrid(new object(), e:null);
            ValueControll();
        }

        private void InitData()
        {
           
            Random rnd = new Random();
            int[] partnerUnits = new int[10];
            int dayLoanCount = 30;
            for (int i = 0; i < partnerUnits.Length; i++)
            {
                do
                {
                    partnerUnits[i] = rnd.Next(10000000, 99999999);
                } while (partnerUnits.Where(e=>e==partnerUnits[i]).Count()>1);
            }
            int k = 0;
            foreach (var item in partnerUnits)
            { 
                for (int i = 1; i < 13; i++)
                {
                    for (int j = 0; j < dayLoanCount; j++)
                    {
                        loans.Add(new LoansGranted()
                        {
                            loanAmount = rnd.Next(0, 1000000000) + (double)(rnd.Next(1, 100)) / 100,
                            loanData = String.Format("2018-{0:00}-01", i),
                            loanNumber = (decimal.Parse("12332534543655756456456456") + k++).ToString(),
                            partnerUnit = item,
                            Pesel = 64041223725
                        }) ;
                    }
                }
            }
            LoanController.SaveOrUpdate();

        }


        private void NumberInputCheck(object sender, KeyPressEventArgs e)
        {
            //Regex re = new Regex("^[0-9]+([,][0-9]{1,4})?$");
            //var newinput = ((TextBox)sender).Text.ToString()+ e.KeyChar.ToString();
            //var match = re.Match(newinput);
            ////if (!(double.TryParse(newinput,out _) && match.Success && match.Length == newinput.Length))
            //if(!match.Success)
            //{ e.Handled = true; }
            //else//jezeli decimal albo , przecinek sprawdzic reszte opszec na text changet
            //{
            //    ((TextBox)sender).Text = match.Value.ToString();
            //    e.Handled = true;
            //}
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            LoanController.chargeACommission(double.Parse(percent.Text),double.Parse(lvl.Text), double.Parse(textBox2.Text));
            RefreshGrid(sender,e);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    var temp = LoanController.loans.Where(z => z.loanData.Remove(7) == periotBox.Text.ToString())
                        .Select(x => x.loanNumber + ";" + x.commission).ToList();
                    File.WriteAllLines(fbd.SelectedPath+"\\Results.txt", temp);
                }
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                    LoanController.loans.Clear();
                    foreach (var loan in File.ReadAllLines(filePath))
                    {
                        LoanController.loans.Add(new LoansGranted(loan));
                    }
                    RefreshGrid(sender,e);
                }
                LoanController.SaveOrUpdate();
            }

        }
    }
}
