﻿using System;
using System.Collections.Generic;
using System.Linq;
using DB.Enties;
using static DB.DB;

namespace WindowsFormsApp1
{
    public static class LoanController
    {
        public static List<LoansGranted> loans { get; set; } = Init();
        public static void SaveOrUpdate()
        {

            using (var s = sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    for (int i = 0; i < loans.Count; i++)
                    {
                        try
                        {
                            s.SaveOrUpdate(loans[i]);
                        }
                        catch (Exception)
                        {
                            //delete reapet element
                            loans.RemoveAt(i);
                        }



                    }
                    t.Commit();
                }
                s.Close();
            }
        }
        
        public static void Truncle()
        {
            using (var s = sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    loans.ForEach(e => s.Delete(e));
                    t.Commit();
                }
                s.Close();
            }
        }

        private static List<LoansGranted> Init()
        {
            List<LoansGranted> l;
            using (var s = sessionFactory.OpenSession())
            {
                l = s.Query<LoansGranted>().ToList();
                s.Close();
            }
            return l;
        }

        public static void chargeACommission(double percent, double lvl, double bonuslvl)
        {

            calculateTheInterestRate();
            var BonusInThePartners = findAndCountBonusPerCredit();
            addTheCostToCredit(BonusInThePartners);

            SaveOrUpdate();

            void calculateTheInterestRate() => loans.ForEach(e => e.CalcCommission(percent));
            double cutAfterTwoZeros(double x) => Math.Floor(x * 100) / 100;

            void addTheCostToCredit(IEnumerable<dynamic> list)
            {
                foreach (var period in list)
                {
                    foreach (var credit in period.loansInMonth)
                    {
                        //credits rest is always n-1 to credit count
                        var handler = loans.Where(e => e.partnerUnit == credit.partner && e.loanData.Remove(7) == period.month).ToList();
                        if (credit.rest>0)
                        {
                        Enumerable.Range(0, (int)(credit.rest * 100)-1)
                                  .ToList()
                                  .ForEach(e=>handler[e].commission+=(decimal)0.01);
                        }

                        
                        loans.Where(e => e.partnerUnit == credit.partner && e.loanData.Remove(7) == period.month)
                                .ToList()
                                .ForEach(e => e.commission +=(decimal) credit.bonusPerCredit);
                    }
                }
            }

            IEnumerable<dynamic> findAndCountBonusPerCredit()
            {
                return loans
                .GroupBy(e => e.loanData.Remove(7))
                .Select(g => new
                {
                    month = g.Key,
                    loansInMonth = g
                                            .GroupBy(e => e.partnerUnit)
                                            .Select(group => new { partner = group.Key, creditCount = group.Sum(c=>1), creditSum = group.Sum(e => e.loanAmount) })
                                             .Where(e => e.creditSum >= lvl)
                                             .Select(bonus => new
                                             {
                                                 bonus.partner,
                                                 bonusPerCredit = cutAfterTwoZeros(bonuslvl / bonus.creditCount),
                                                 rest =(decimal)bonuslvl- (decimal)(cutAfterTwoZeros(bonuslvl / bonus.creditCount) * bonus.creditCount)
                                             })
                                             .ToList()
                });
            };

        }
    }
}
